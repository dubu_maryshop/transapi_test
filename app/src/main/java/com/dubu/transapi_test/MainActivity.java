package com.dubu.transapi_test;

import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private String TAG = "transapi_test";

    public static EditText Edittext1;
    public static TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Edittext1 = (EditText)findViewById(R.id.editText1);
        textView = (TextView)findViewById(R.id.textView);

        Edittext1.setFocusable(false);
        Edittext1.setFocusableInTouchMode(false);

        final Button test1But = (Button) findViewById(R.id.test1);
        test1But.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetTrans2().execute("很高興認識你","Taiwan","English");
            }
        });


        final Button test2But = (Button) findViewById(R.id.test2);
        test2But.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetTrans().execute("早安","Chinese","Japanese");
            }
        });


        final Button test3But = (Button) findViewById(R.id.test3);
        test3But.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetUpdate().execute("updatedb","t20 > 1");
            }
        });

    }

    private class GetTrans extends AsyncTask<String, String, String> {
        //String result = "";
        String Estr = "";
        @Override
        protected void onPreExecute() {
//            startAsync = SystemClock.elapsedRealtime();
//            outputTextView.setText("\n\n");
        }

        @Override
        protected String doInBackground(String... params) {

            String nameSpace = "http://tempuri.org/";
            String methodName = "Translate";
            String WEB_SERVICE_URL = "http://trans.dubugroup.com/dubutrans.asmx?wsdl";

            String soapAction = "http://tempuri.org//Translate/";

            Estr = params[0] + " : " + params[1] + " to " + params[2];

            SoapObject request  = new SoapObject(nameSpace, methodName);
            //2.设置调用方法的参数值
            request.addProperty("text", params[0]);
            request.addProperty("lang", params[1]);
            request.addProperty("langTo", params[2]);

            // 3.生成调用WebService方法的SOAP请求信息
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
            envelope.bodyOut = request;
            // c#写的应用程序必须加上这句
            envelope.dotNet = true;
            HttpTransportSE ht = new HttpTransportSE(WEB_SERVICE_URL);
            try {
                ht.call(soapAction,envelope);
            } catch (HttpResponseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }

            try {
                final SoapPrimitive result = (SoapPrimitive) envelope.getResponse();
                if (result != null) {
                    //返回的結果


                    Log.d(TAG, result.toString());
                    return result.toString();
                }
            } catch(SoapFault e){
                Log.e(TAG, e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result){
            Edittext1.setText(Estr);
            textView.setText(result);
        }

    }

    private class GetTrans2 extends AsyncTask<String, String, String> {
        //String result = "";
        String Estr = "";
        @Override
        protected void onPreExecute() {
//            startAsync = SystemClock.elapsedRealtime();
//            outputTextView.setText("\n\n");
        }

        @Override
        protected String doInBackground(String... params) {

            String nameSpace = "http://tempuri.org/";
            String methodName = "Translate";
            String WEB_SERVICE_URL = "http://assistant.dubugroup.com/voiceassistant.asmx?wsdl";

            String soapAction = "http://tempuri.org//Translate/";

            Estr = params[0] + " : " + params[1] + " to " + params[2];

            SoapObject request  = new SoapObject(nameSpace, methodName);
            //2.设置调用方法的参数值
            request.addProperty("text", params[0]);
            request.addProperty("lang", params[1]);
            request.addProperty("langTo", params[2]);

            // 3.生成调用WebService方法的SOAP请求信息
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
            envelope.bodyOut = request;
            // c#写的应用程序必须加上这句
            envelope.dotNet = true;
            HttpTransportSE ht = new HttpTransportSE(WEB_SERVICE_URL);
            try {
                ht.call(soapAction,envelope);
            } catch (HttpResponseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }

            try {
                final SoapPrimitive result = (SoapPrimitive) envelope.getResponse();
                if (result != null) {
                    //返回的結果


                    Log.d(TAG, result.toString());
                    return result.toString();
                }
            } catch(SoapFault e){
                Log.e(TAG, e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result){
            Edittext1.setText(Estr);
            textView.setText(result);
        }

    }

    private class GetUpdate extends AsyncTask<String, String, String> {
        //String result ;
        String Estr = "";
        @Override
        protected void onPreExecute() {
//            startAsync = SystemClock.elapsedRealtime();
//            outputTextView.setText("\n\n");
        }

        @Override
        protected String doInBackground(String... params) {

            String nameSpace = "http://tempuri.org/";
            String methodName = "Updateinfo";
            String WEB_SERVICE_URL = "http://assistant.dubugroup.com/voiceassistant.asmx?wsdl";

            String soapAction = "http://tempuri.org//Updateinfo/";

            Estr = params[0] + " : " + params[1] ;

            SoapObject request  = new SoapObject(nameSpace, methodName);

            //2.设置调用方法的参数值
            request.addProperty("Tb", params[0]);
            request.addProperty("Wstr", params[1]);

            // 3.生成调用WebService方法的SOAP请求信息
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
            envelope.bodyOut = request;
            // c#写的应用程序必须加上这句
            envelope.dotNet = true;
            HttpTransportSE ht = new HttpTransportSE(WEB_SERVICE_URL);
            try {
                ht.call(soapAction,envelope);
            } catch (HttpResponseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }

            try {
                final SoapPrimitive result = (SoapPrimitive) envelope.getResponse();
                if (result != null) {
                    //返回的結果


                    Log.d(TAG, result.toString());
                    return result.toString();
                }
            } catch(SoapFault e){
                Log.e(TAG, e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result){
            Edittext1.setText(Estr);
            textView.setText(result);
        }

    }

}
